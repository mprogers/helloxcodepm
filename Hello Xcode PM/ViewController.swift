//
//  ViewController.swift
//  Hello Xcode PM
//
//  Created by Michael Rogers on 1/31/19.
//  Copyright © 2019 Michael Rogers. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var phraseLBL: UILabel!
    
    @IBOutlet weak var nameTF: UITextField!

    @IBAction func clickMe(_ sender: Any) {
        let phrases = ["Hello", "Namaste", "Howdy", "Bonjour", "Ola"]
        
        let index = Int.random(in: 0 ..< phrases.count)
        
        phraseLBL.text = "\(phrases[index]), \(nameTF.text!)"
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

